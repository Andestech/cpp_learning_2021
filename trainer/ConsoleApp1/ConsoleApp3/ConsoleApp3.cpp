﻿#include <iostream>
#include <bitset>
using namespace std;

void swap1(int* const p1, int* const p2) {
	int c = *p2;
	*p2 = *p1;
	*p1 = c;
}

void swap2(int& p1, int& p2) {
	int c = p2;
	p2 = p1;
	p1 = c;
}



int main()
{
    //pointers
	int N = 100;
	int* ptr = nullptr;

	cout << "ptr =" << ptr << endl;

	ptr = &N;

	cout << "&N =" << ptr << ", &ptr=" << &ptr << endl;
	*ptr = 500;
	cout << "N =" << *ptr << endl;
	cout << "N =" << N << endl;

	// const
	const int Nc = 100;
	const int Nc2 = 200;
	// poinetr to const int
	int const * ptr2 = &Nc;
	 // *ptr2 = 12;
	cout << "Nc =" << *ptr2 << endl;
	ptr2 = &Nc2;
	cout << "Nc2 =" << *ptr2 << endl;

	//const pointer to int
	int Nc3 = 300, Nc4 = 400;
	int* const ptr3 = &Nc3;
	cout << "Nc3 =" << *ptr3 << endl;
	//ptr3 = &Nc4;

	//const pointer to const int
	int Nc5 = 100;
	int const * const ptr4 = &Nc5;
	cout << "Nc5 =" << *ptr4 << endl;
	// *ptr4 = 111; //forbidden!

	//swap test
	int a = 10, b = 70;
	cout << "a=" << a << ", b=" << b << endl;
	swap1(&a, &b);
	cout << "a=" << a << ", b=" << b << endl;

	// references

	int& ref1 = Nc5;
	cout << "a=" << a << ", b=" << b << endl;
	swap2(a, b);
	cout << "a=" << a << ", b=" << b << endl;

	// const 
	int data = 0;
	//cout << "data: "; cin >> data;
	const int data2 = data;
	constexpr int ce = 1000;

	// lab test (bit inversion)
	// инвертируем младший байт

	int m = 0b1011'1111'0101;
	unsigned int mask = -1;

	cout << "m=" << bitset<32>(m) << endl;
	int m2 = ((mask << 24) >> 24);

	cout << "m2=" << bitset<32>(m2) << endl;

	int m3 = m2^m;
	cout << "m3=" << bitset<32>(m3) << endl;
	// to be continued...

	int p = 10, n = 5;


	unsigned int res1 = mask << (sizeof(int) * 8 - p);
	unsigned int res2 = res1 >> (sizeof(int) * 8 - p);
	unsigned int res3 = res2 >> (p-n);
	unsigned int res4 = res3 << (p-n);
	
	cout << " m=" << bitset<32>(m) << endl;
	int res5 = m ^ res4;
	cout << "*m=" << bitset<32>(res5) << endl;

	return 0;
}
