﻿#include <iostream>

/*
test prog 1
*/

using namespace std;

void showGreets() {
	cout << "Hello World!" << endl;
}

int sum(int a, int b)
{
	cout << "call int.. ";
	return a + b;
}

double sum(double a, double b)
{
	cout << "call double.. ";
	return a + b;
}


void showTypes() {
	// fundamental types of c++
	bool b1 = true;
	cout << "sizeof(bool)=" << sizeof(bool) << endl;
	char ch = 65;
	cout << "sizeof(char)=" << sizeof(char) << ", ch=" << ch << endl;
	cout << "sizeof(wchar_t)=" << sizeof(wchar_t) << endl;
	cout << "sizeof(char32_t)=" << sizeof(char32_t) << endl;
	cout << "sizeof(short int)=" << sizeof(short int) << endl;
	cout << "sizeof(int)=" << sizeof(int) << endl;
	unsigned int n2 = (1 << 31) - 1;

	long long NN = 8'888'888'888'888;
	cout << "NN=" << NN << endl;
	cout << "n2=" << n2 << endl;
	cout << "sizeof(long int)=" << sizeof(long int) << endl;
	cout << "sizeof(long long int)=" << sizeof(long long int) << endl;

	// __uint128_t

	cout << "sizeof(float)=" << sizeof(float) << endl;
	cout << "sizeof(double)=" << sizeof(double) << endl;
	cout << "sizeof(long double)=" << sizeof(long double) << endl;
}


int main()
{
	showGreets();
	cout << "1 + 3 = " << sum(1, 3) << endl;
	cout << "1.1 + 3.22 = " << sum(1.1, 3.22) << endl;

	int N = 12, A=1234, C=234555;
	//showTypes();

	int v1 = 0xaffe334;
	int v2 = 0120;
	int v3 = 0b011101111;

	// & | ^ ~ << >>
	cout << "51/4 = " << (51 >> 2) << endl;



}
