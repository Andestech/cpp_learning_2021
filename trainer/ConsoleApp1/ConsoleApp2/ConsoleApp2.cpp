﻿#include <iostream>
#include "t3.h";


void f2();

using namespace std;



int main()
{
	int i = 2;
	if (i == 2) { cout << "Ok" << endl; }
	else { cout << "False" << endl; }


	if (i != 2) { cout << "Ok" << endl; cout << "extra data" << endl; }
	else { cout << "False" << endl; }

	cout << ((i != 2) ? "TRUE!!" : "FALSE!") << endl;


	short int mark = 3;

	for (;;) {
		cout << "enter mark: "; cin >> mark;
		if (mark == -1) break;
		switch (mark)
		{
		case 0:
		case 1:
		case 2: cout << "Very Bad!!" << endl; break;
		case 3:
		case 4: cout << "Some(" << endl; break;
		case 5:
		case 6:
		case 7: cout << "Good" << endl; break;
		case 8:
		case 9: cout << "Very good!" << endl; break;
		case 10: cout << "Excellent!" << endl; break;
		default: cout << "Not valid mark.. try again!" << endl;
		}

	}

	{ int k = 10; }

	int k = 0;


 // cycles

	for (int i = 0; i < 5; i++) {
		/*...*/
		if (i % 2 == 0) continue;
		cout << "i=" << i << endl;
	}

	{   int i = 0;
	for (;;) { if (i >= 5) break; /*...*/ i++; }
	}

	for (; k < 5; k++) {
	}

	while (k>=0) 
	{
		cout << "k=" << k-- << endl;
	}

	do
	{
		cout << "++ enter mark: "; cin >> mark;
	} while (mark != -1);

	f2();
	Fnms::f3();

}

