﻿#include <iostream>


using namespace std;

struct container {
	int* ptr;
	container() { ptr = new int; cout << "+++ ctor container: " << this << endl;}
	virtual ~container() { delete ptr; cout << "--- dtor container: " << this << endl;}
};

struct extra : container
{
	int* ptr2;
	extra() { ptr2 = new int; cout << "+++ ctor extra: " << this << endl; }
	~extra() { delete ptr2; cout << "--- dtor extra: " << this << endl; }

};

struct extra2
{
	int* ptr2;
	container* cnt;
	extra2() { cnt = new container;  ptr2 = new int; cout << "+++ ctor extra: " << this << endl; }
	~extra2() { delete cnt;  delete ptr2; cout << "--- dtor extra: " << this << endl; }

};


class A {
public: 
	int a1;
	//A() = delete;
	A();
	A(int a, int b);
	void f() { cout << "A f call " << endl; };
	void f(int a) { cout << "A f(a) call " << endl; };
	virtual void getInfo() { cout << "A info: a1=" << a1 << ", a2 =" << a2<< endl; } // inline
	//void f(int a, int b) {};
	~A(){ cout << "--- dtor A: " << this << endl; } // inline

private:
	int a2;

protected:
	int a3;

};

A::A(int a, int b) :a1(a), a2(b), a3(b) { cout << "+++ ctor A: " << this << endl; }
A::A():a1(1),a2(1),a3(1){ cout << "+++ base ctor A: " << this << endl; }

class B final: public A {

public:
	B();
	B(int a, int b);
	void testVars() {
	}
	void f() { cout << "B f call " << endl; }
	void f(int a) { A::f(a); }
	void f2(int a) {cout << "B f2(a) call: " << a << endl; }
	void getInfo() override { cout << "B: "; A::getInfo(); }
	~B() { cout << "--- dtor B: " << this << endl; } // inline
};

B::B(int a, int b):A(a,b) { cout << "+++ ctor B: " << this << endl; }
B::B() { cout << "+++ base ctor B: " << this << endl; }

void testReference(A& a)
{
	a.getInfo();
}

void testAB() {
	A a(1, 2);
	B b(1, 3);
	
	cout << "------------------------------------------" << endl;
	B b3;
	b3.f();
	b3.f(1);
	b3.f2(1);
	cout << "------------------------------------------" << endl;

	testReference(a);
	testReference(b);

	A* ptr_a = new B;
	ptr_a->getInfo();

	((B*)ptr_a)->f2(1);

	delete ptr_a;
	cout << "------------------------------------------" << endl;
}


void test(container& c) {
}

int main()
{

{
		extra ex;
}

cout << "----------------------------------------" << endl;

container* ex = new extra;
/*   */
delete ex;

cout << "+++++++++++++++++++++++++++++++++++++++++++" << endl;

extra2 ex2;

cout << ex2.cnt->ptr << endl;



}
