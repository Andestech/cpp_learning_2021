﻿#include <iostream>

using namespace std;

struct Figure
{
	virtual void move_to_x(int from, int to)=0;
	virtual void move_to_y(int from, int to)=0;
	virtual void rotate(int rad)=0;

};

struct Circ : Figure {
	void move_to_x(int from, int to) { cout << "move_X Circ = " << from << "-->"<< to << endl; }
	void move_to_y(int from, int to) { cout << "move_Y Circ = " << from << "-->" << to << endl; }
	void rotate(int rad) { cout << "rotate Circ = " << rad << endl; }

};

struct Rectangle : Figure {
	void move_to_x(int from, int to) { cout << "move_X rectangle = " << from << "-->" << to << endl; }
	void move_to_y(int from, int to) { cout << "move_Y rectangle = " << from << "-->" << to << endl; }
	void rotate(int rad) { cout << "rotate rectangle = " << rad << endl; }

};


void move_to(Figure& figure) // omit coords...
{
	figure.move_to_x(10, 20);
	//.........
	figure.move_to_y(15, 30);


}

int main()
{
	// Figure f; ошибка для класса ABC
	Circ c1;
	Rectangle r1;

	move_to(c1);
	move_to(r1);
}

