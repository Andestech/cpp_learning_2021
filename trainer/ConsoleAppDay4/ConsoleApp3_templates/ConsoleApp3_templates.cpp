﻿#include <iostream>

using namespace std;


void f0(int a)
{
	cout << a << endl;
}

template<typename T>
void f(T a)
{
	cout << a << endl;
}

template<class T>
void swap1(T& a1, T& a2 ) {
	T c;
	c = a1;
	a1 = a2;
	a2 = c;
}

template<class T>
class container
{
private:
	T data;
public:
	container(T d):data(d) {}
	T getData() const { return data; }
	void setData(T t) { data = t; }
};


struct A { int b; };


// variant 1
template<class T>
class safe_array
{
private:
	T* ptr;
	int size;

public:
	safe_array(int s) :size(s) { ptr = new T[size]; }
	T& operator[](int index)
	{
		if (index >= size || index < 0) { cout << "array index out out of bounds: " << index << endl; exit(0); }
		return ptr[index];
	}
	
};

// variant 2
template<class T = int, int size = 5>
class safe_array2
{
private:
	T* ptr;
	//int size;

public:
	safe_array2() { ptr = new T[size]; }
	T& operator[](int index)
	{
		if (index >= size || index < 0) { cout << "array index out out of bounds: " << index << endl; exit(0); }
		return ptr[index];
	}

};


template<class T, int size>
void echo_array(T(&array)[size])
{
	for (int i = 0; i < size; i++) cout << array[i] << " ";
	cout << endl;
}


int main()
{
   
	f(10);
	f(10.1);
	f("AAAA");
	A a1 { 1 };
	A a2 { 100 };
	//f(a);
	int i1 { 1 };

	int i2 = 1, i3 = 10;
	cout << i2 << " --- " << i3 << endl;
	swap1(i2, i3);
	cout << i2 << " --- " << i3 << endl;
	cout << a1.b << " --- " << a2.b << endl;
	swap1(a1, a2);
	cout << a1.b << " --- " << a2.b << endl;
	
	container<int> c1 = 1;
	container<A> c2({ 1000 });
	cout << c2.getData().b << endl;
	cout << "-----------------------------------------------" << endl;

	safe_array<int> arr1(5);
	arr1[0] = 10;
	arr1[2] = 20;
	arr1[4] = 30;

	//arr1[5] = 30;  // error!!
	for (int i = 0; i < 5; i++) cout << arr1[i] << endl;

	safe_array<A> arr2(3);
	arr2[0] = {-1};
	arr2[1] = {-2};
	arr2[2] = { -3 };

	for (int i = 0; i < 3; i++) cout << arr2[i].b << endl;

	safe_array2<A,2> arr3;
	arr2[0] = { -100 };
	arr2[1] = { -200 };
	
	for (int i = 0; i < 2; i++) cout << arr3[i].b << endl;

	safe_array2<> arr5;

	int arr7[] = { 1,2,3,4,5 };
	echo_array(arr7);


}
