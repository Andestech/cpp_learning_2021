﻿
#include <iostream>
#include <cmath>

using namespace std;

class A {

	int var1, var2;
};



struct B0 {
	int var1;
	int var2;
};


struct B {

private:
	int var3;

public:
	int var1;
	int var2;


};


B0 genB(int a, int b)
{
	return { a,b };
}


B genB2(int a, int b)
{   
	B sb; 
	sb.var1 = a; 
	sb.var2 = b;
	return sb;
}


B& genB3(int a, int b)
{
	B sb;
	sb.var1 = a;
	sb.var2 = b;
	return sb;
}

B genB4(int a, int b)
{
	B sb;
	sb.var1 = a;
	sb.var2 = b;
	return sb;
}

void testStructs() {
	A a;
	B b; B0 b2;

	b.var1 = 11;
	b.var2 = 22;

	b2 = { 1,20 };

	cout << "var1=" << b2.var1 << ", var2=" << b2.var2 << endl;

	//B b4 = genB3(10, -10); // bad code!!!! cl.exe - ok!!
	B b4 = genB4(10, -10);
	cout << "var1=" << b4.var1 << ", var2=" << b4.var2 << endl;

}

static const double PI = 3.1415;


class Circ
{
private:
	double r;
	//static const double PI;

public:
	Circ();
	Circ(double r);
	Circ(const Circ& circ);
	~Circ();
	Circ& operator=(const Circ& c2);
	Circ operator+(const Circ& c2);
    friend bool operator==(const Circ& c1, const Circ& c2); // учебный пример
	//bool operator==(const Circ& c2); // более правильная альтернатива
	bool operator<=(const Circ& c1);
	//friend void operator<<(const ostream& o, const Circ& c2);
	Circ operator+(int a);

	double getR() const;
	double getS() const;
	double getP() const;
	void setR(double val);
};

//bool operator==(const Circ& c1, const Circ& c2);


//Circ::Circ(double rad):r(rad) {}
//Circ::Circ() : r(1) {}
//Circ::Circ(const Circ& circus) : r(circus.r) {}

// add var create demo...

Circ::Circ(double rad) :r(rad) { cout << "+++ ctor:  " << this << endl; }
Circ::Circ() : Circ(1) {}
Circ::Circ(const Circ& circus) : Circ(circus.r) {}
Circ::~Circ() { cout << "--- dtor:  " << this << endl; }
Circ& Circ::operator=(const Circ& c2) { cout << "= operator call" << endl; r = c2.r; return *this; }
Circ Circ::operator+(const Circ& c2) { Circ res; res.r = sqrt(r * r + c2.r * c2.r); return res; };

// bool operator==(const Circ& c1, const Circ& c2) { return c1.getR() == c2.getR(); }
bool operator==(const Circ& c1, const Circ& c2) { return c1.r == c2.r; }
bool Circ::operator<=(const Circ& c1) { return r <= c1.r; };

double Circ::getR() const { return r; }
double Circ::getS() const { return PI * r * r; }
double Circ::getP() const { return PI * r * 2; }
void Circ::setR(double val) { r = val; }

//void operator<<(const ostream& o, const Circ& c4) {
//	cout << "Circ: r=" << c4.getR() << ", S=" << c4.getS() << ", P=" << c4.getP();
//};

ostream& operator<<(ostream& o, const Circ& c4) {
	cout << "Circ: r=" << c4.getR() << ", S=" << c4.getS() << ", P=" << c4.getP();
	return o;
};

Circ operator+(int a, Circ c) { Circ c2;  c2.setR(c.getR() + a); return c2; }
Circ Circ::operator+(int a) { Circ c2;  c2.setR(getR() + a); return c2; }


void testCirc() {
	
	Circ c1;
	//c1.setR(10);
	cout << "Circ: r=" << c1.getR() << ", S=" << c1.getS() << ", P=" << c1.getP() << endl;

	Circ c2(10);
	//c1.setR(10);
	cout << "Circ: r=" << c2.getR() << ", S=" << c2.getS() << ", P=" << c2.getP() << endl;

	Circ c3 = 100;
	//c1.setR(10);
	cout << "Circ: r=" << c3.getR() << ", S=" << c3.getS() << ", P=" << c3.getP() << endl;

	Circ c4 = c3;
	//c1.setR(10);
	cout << "Circ: r=" << c4.getR() << ", S=" << c4.getS() << ", P=" << c4.getP() << endl;

	Circ c5(c4);
	//c1.setR(10);
	cout << "Circ: r=" << c5.getR() << ", S=" << c5.getS() << ", P=" << c5.getP() << endl;

	Circ c6(21);
	cout << addressof(c6) << " ----" << addressof(c5) << endl;
	cout << "Circ: r=" << c6.getR() << ", S=" << c6.getS() << ", P=" << c6.getP() << endl;

	c5 = c6;
	cout << addressof(c6) << " ----" << addressof(c5) << endl;
	cout << "Circ: r=" << c5.getR() << ", S=" << c5.getS() << ", P=" << c5.getP() << endl;
	cout << c5; cout << endl;


	Circ c7;
	c7 = c3 + c2;
	cout << (c7 == c2) << endl;
	cout << (c7 <= c2) << endl;

	cout << "+++++++-----------------------------------+++++++++++" << endl;

	c3 = 1;

	cout << "### "; cout << c3; cout << endl;
	//cout << "### " << c3 << endl; ДЗ. реализовать!
	cout << "### " << c3 << endl;

	c3 = c3 + 2;
	cout << "### " << c3 << endl;
	c3 = 2 + c3;
	cout << "### " << c3 << endl;

}

class Stest
{
public:
	int var;
	int* ptr;

	Stest();
	Stest(const Stest& st);
	~Stest();
	Stest& operator=(const Stest& st);
};

Stest::Stest() { /**/ cout << "+++ ctor:  " << this << endl; var = 10; ptr = new int; /*...*/ }
Stest::~Stest() { cout << "--- dtor:  " << this << endl; delete ptr; }
Stest::Stest(const Stest& st) { var = st.var;  ptr = new int; *ptr = *st.ptr; }
Stest& Stest::operator=(const Stest& st) { var = st.var;  ptr = new int; *ptr = *st.ptr; return *this; }


void testDeepCopy()
{
	
	cout << "-----------------------------------" << endl;

	Stest s1;

	*s1.ptr = 100;

	Stest s2 = s1;
	cout << addressof(s1) << "-----" << addressof(s2) << endl;
	cout << &s1 << "-----" << &s2 << endl;


	*s2.ptr = 1;


	cout << *s1.ptr << "-----" << *s2.ptr << endl;
	s1 = s2;
}


int main()
{
	testCirc();
	//testDeepCopy();


	// пример размещения переменной типа B0 в куче
	B0* b0 = new B0;
	b0->var1 = 10;
	b0->var2 = 20;

	delete b0;
	
}

