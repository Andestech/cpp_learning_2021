﻿#include <iostream>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <deque>



using namespace std;

int main()
{

	string s1 = "AAAAAAA";
	cout << "size= " << s1.length() << endl;
	cout << "max= " << s1.max_size() << endl;
	s1.reserve(30);
	s1.append("CCCC");
	s1.insert(9, 30, '+');

	//s1[10] = 'B';
	cout << s1 << endl;
	cout << s1.at(15) << endl;
	cout << "capacity= " << s1.capacity() << endl;

	//---------------------------------------


	vector<int> v(10);
	for (auto& a : v) cout << (a = rand()%100) << endl;
	cout << "-------------------------" << endl;
	for (auto& a : v) cout << a << endl;
	cout << "capacity= " << v.capacity() << endl;
	v.push_back(100);
	cout << "capacity= " << v.capacity() << endl;

	cout << "-------------------------" << endl;
	stack<string> st1;
	st1.push("AA");
	st1.push("BB");
	st1.push("CC");
	st1.push("DD");

	//----------------------------------------------------------
	queue<int> q1;  // stack like

	deque<int> dq1;

	dq1.push_back(10);
	dq1.push_back(20);
	dq1.push_front(-10);
	dq1.push_front(-20);

	for (auto& q : dq1) cout << q << " ";
	cout << endl;




}

