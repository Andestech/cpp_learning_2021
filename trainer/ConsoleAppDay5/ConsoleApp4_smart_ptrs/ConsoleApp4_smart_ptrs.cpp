﻿
#include <iostream>

using namespace std;

struct S
{
    int* data;
    S(int a) { data = new int; *data = a; cout << "+++ ctor " << this << endl; }


    ~S() { delete data;  cout << "--- dtor " << this << endl;
    }
};


void test_smart() {

    unique_ptr<S> ptr(new S(23));
    //....

}


int main()
{

    int* a = new int(100);
   
    unique_ptr<int> ptr(new int(23));

    unique_ptr<int> ptr2;

    //ptr2 = ptr; // compile error

    *ptr = 100;
    cout << "addr: " << ptr << ", *ptr=" << *ptr << endl;

    ptr2 = move(ptr);
    cout << "addr ptr: " << ptr  << endl;
    cout << "addr ptr2: " << ptr2 << ", *ptr2=" << *ptr2 << endl;

    delete a;

    test_smart();

}
