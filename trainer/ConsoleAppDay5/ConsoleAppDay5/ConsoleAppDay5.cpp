﻿
#include <iostream>
using namespace std;


struct baseA {
    int varA;
    virtual void getInfo() {}
    baseA() { varA = 10; }

};

struct B : public baseA
{
    long double varB;
    B() { varB = 1.123456789; }

};

struct C : public baseA
{
    int varC;
    C() { varC = -100; }
};


void test(baseA* b)
{
//.....

    C* c = dynamic_cast<C*>(b);
    if(c != nullptr)
    cout << "test! c: " << c->varC << endl;

}


class my_integer
{
private:
    int N;
    char* buf;

public:
    my_integer(int v):buf(nullptr),N(v) {
        if (N < 0 || N> 10) exit(0);
    };
    ~my_integer() {
        if (buf != nullptr) delete buf;
    };
    operator const char* () {
        static const char* out[] = {"zero", "one", "two"};
        return out[N];
    
    };



};



int main()
{
    //static
  
    int a = 1000'000'000;
    short b = (short)a; // int(a)
    short c = static_cast<short>(a);
    cout << "b=" << b << endl;
    cout << "c=" << c << endl;

    B* b1 = (B* )( new C);
    cout << b1->varB << endl;

    B* b2 = new B;
    cout << b2->varB << endl;

    //B* b3 = static_cast<B*>(new C); // compile error!!!
    //cout << b3->varB << endl;

     //-------------------------------------
    // RTTI

    C* c1= new C;
    test(c1);

    test(b2);  


    delete b2;
    delete b1;

   // '1','2','3'   //var1
   // 'one', 'two'

    int i1 = 6;
    char ch1 = i1 + '0';

    cout << ch1 << endl;

    // my_integer i9(20);

    // test Int
    my_integer i0(2);
    cout << (const char*)i0 << endl;
   




}

