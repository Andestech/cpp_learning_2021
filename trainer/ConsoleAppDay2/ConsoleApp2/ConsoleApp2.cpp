﻿#include <iostream>
#include <typeinfo>

using namespace std;


auto f1(int a) -> decltype(a + 2.0L)
{
    return a;
}

void test_return_type() {
    cout << "sizeof(f1)=" << sizeof(f1(1)) << endl;
    long long a = 1;
    cout << typeid(a).name() << endl;
    cout << typeid(f1(1)).name() << endl;
}


void check_input()
{

    char answers[4][10];

    for (int j = 0; j < 4; j++)
    {
        cout << "Enter answer: ";
        cin.getline(answers[j], 10);
    }
    cout << endl;
    for (int j = 0; j < 4; j++) cout << answers[j] << endl;
}

int main()
{
    char str1[] = "abc";
    char str2[] = {'a','b','c','\0'};
    const char str3[] = "HHH";
    const char str4[] = { 'a','b','c','\0' };

    char* p1 = str1;
    const char* p2 = str3;

    const char* p3 = "efg";
    //char* p4 = "efg"; // compile error

    cout << str1[2] << endl;
    cout << "strlen(str1)=" << strlen(str1) << endl;
    str1[3] = 'e';
    cout << str1 << endl;
    cout << "strlen(str1)=" << strlen(str1) << endl;

   // strcpy(str1, str3); // g++ - ok
   // strcpy(str2, str3); //ms - ok
    cout << str2 << endl;
    strncpy(str2, str3, 2); //ms - ok
    cout << str2 << endl;

    // search
    char greets[] = "How do you do?!!";
    char* ptrs = strchr(greets, 'y');
    cout << ptrs << endl;

    //
    char* ptrs2 = strstr(greets, "1ou");
    if(ptrs2!=nullptr)
    cout << ptrs2 << endl;

    //cmp
    char greets2[] = "How do you do?!";
    cout << strcmp(greets, greets2) << endl;

    //concat
    char greets3[50] = "Hey you! ";
    strcat(greets3, greets2);
    cout << greets3 << endl;

    //tokenizing

    char data[] = "123, 55, 55,6,33,11,22,344,55,66,777,8,9,4";
    char* d = strtok(data, ", ");
    while (d)
    {
        cout << d << endl;
        d = strtok(NULL, ", ");

    }
    cout << data;




    const char* str00 = new char[10];
    //.....
    delete[] str00;

    
}

