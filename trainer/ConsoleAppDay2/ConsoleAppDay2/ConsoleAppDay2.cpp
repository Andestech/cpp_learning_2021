﻿#include <iostream>

using namespace std;

// Array intro


int sum1(int a[]) {
	int s = 0;
	unsigned int size = sizeof(a) / sizeof(*a);
	for (int i = 0; i < size; i++) s += a[i];
	return s;
} // не работает

int sum2(int a[], int size) {
	int s = 0;
	for (int i = 0; i < size; i++) s += a[i];
	return s;
} // работает


void echo_array(int a[][3], int rows)
{
	for (int i = 0; i < rows; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			cout << a[i][j] << " ";
		}
		cout << endl;
	
	}

}


void echo_array(int** a, int rows, int cols)
{
	for (int i = 0; i < rows; i++)
	{
		for (int j = 0; j < cols; j++)
		{
			cout << a[i][j] << " ";
		}
		cout << endl;

	}

}


void fill_array(int (*a)[3], int rows)
{
	for (int i = 0; i < rows; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			a[i][j] = -100 + (rand() % 201 );
		}
		cout << endl;

	}

}

void fill_array(int**a, int rows, int cols)
{
	for (int i = 0; i < rows; i++)
	{
		for (int j = 0; j < cols; j++)
		{
			a[i][j] = -100 + (rand() % 201);
		}
		cout << endl;

	}

}


void echo_array2(int (*a)[3], int rows)
{}

int main()
{
	int a = 0;
	int arr[10];

	const int cap = 10;
	int arr2[cap];

	cout << "&arr[0] =" << &arr[0] << endl;
	cout << "&arr[0] =" << arr << endl;
	cout << "arr[0] =" << arr[0] << endl;

	int arr3[] = {11,22,33,44,55,66};
	int arr4[] = { 77,78,79,80 };

	for (int i = 0; i < cap; i++) { arr2[i] = i; cout << arr2[i] << ", "; }
	cout << endl;
	for (int i = 0; i < cap; i++) { *(arr2 + i) = i*i; cout << arr2[i] << ", "; }
	cout << endl;
	for (int i = 0; i < cap; i++) { *(arr2 + i) = i * i; cout << &arr2[i] << endl; }
	cout << endl;


	int N2 = 2;
	// cout << "N2="; cin >> N2;
	// int arr4[N2]; error in stack!!

	cout << "arr3 size: " << (sizeof(arr3) / sizeof(*arr3)) << endl;
	cout << "arr3 sum =" << sum1(arr3) << endl;
	cout << "arr3 sum =" << sum2(arr3, sizeof(arr3) / sizeof(*arr3)) << endl;

	for (int j = 0; j < 10; j++) cout << *(arr3 + j) << ", addr: " << &arr3[j] << endl;

	int* ptr = arr3;

	cout << "------------------------------" << endl;
	for (auto& i : arr4) { i++;  cout << i << endl; }
	cout << "------------------------------" << endl;
	for (auto i : arr4)  cout << i << endl;
	cout << "------------------------------" << endl;
	for (auto i : {'w','d','A'})  cout << i << endl;
	cout << "------------------------------" << endl;

	// 00BAFB54
	//void* ptr2 = (void*)0x00BAFB54;
	//int* ptr3 = (int*)0x007AF698;
	//for (int j = 0; j < 6; j++) cout << *(ptr3 + j) << ", addr: " << &ptr3[j] << endl;
	//int arr5[1000'000]; // проблема Stack

	int* arr5 = new int[50'000'000];
	/*
	to do
	*/

	delete [] arr5;


	// 2d ARRAYS

	int mtrx[2][3] = { {1,2,3},{4,5,6} };
	echo_array(mtrx,2);

	// heap reliaze
	int row = 10, col = 3;
	int** mtrx2 = new int* [row];
	for (int i = 0; i < row; i++) mtrx2[i] = new int[col];

	mtrx2[1][2] = 1222;

	// code
	fill_array(mtrx2, 10, 3);
	echo_array(mtrx2, 10, 3);


	for (int i = 0; i < row; i++) delete [] mtrx2[i];
	delete[] mtrx2;
}
